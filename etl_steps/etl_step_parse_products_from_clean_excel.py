# coding=utf-8

"""This module, etl_step_parse_products_from_clean_excel.py, will read in the Excel data into Python objects."""

# Parent class.
from etl_steps import etl_step
# Used for threading.
import threading
# Used to read Excel files.
from csv_utilities import excel_utilities
# Used to get useful paths.
import path_manager as pm


class ElectricalETLStepParseProductsFromCleanExcel(etl_step.ElectricalETLStep):
	"""This ETL step will return a list of Product objects."""

	def __init__(self):
		self.etl_step_name = etl_step.ETL_STEP_PARSE_PRODUCTS_FROM_CLEAN_EXCEL
		self.excel_parser  = None
		threading.Thread.__init__(self)
		super().__init__(self.etl_step_name)

	def run(self):
		"""Run this ETL step."""
		self._start_timer()
		self.excel_parser = excel_utilities.ElectricalExcelParser(pm.EXCEL_CLEANED_PATH)
		self.stdout       = self.excel_parser.parse_out_products()
		self._stop_timer()

