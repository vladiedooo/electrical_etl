# coding=utf-8

"""This module, etl_step_parse_products_from_clean_excel.py, will read in the Excel data into Python objects."""

# Parent class.
import threading

import path_manager as pm
from dam import electrical_dam_parser
from etl_steps import etl_step


class ElectricalETLStepParseDAMAssetInformation(etl_step.ElectricalETLStep):
	"""This ETL step will return a list of Product objects."""

	def __init__(self):
		self.etl_step_name = etl_step.ETL_STEP_PARSE_DAM_ASSET_INFORMATION
		self.dam_parser  = None
		threading.Thread.__init__(self)
		super().__init__(self.etl_step_name)

	def run(self):
		"""Run this ETL step."""
		self._start_timer()
		self.dam_parser = electrical_dam_parser.ElectricalDAMParser(pm.DAM_ORIGINAL_DIRECTORY, pm.DAM_CLEANED_DIRECTORY)
		self.stdout     = self.dam_parser.parse_out_assets()
		self._stop_timer()


