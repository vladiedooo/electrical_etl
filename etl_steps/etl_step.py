# coding=utf-8

"""This module, electrical_etl_step.py, is used to define con-current modules (ETL steps) to run in parallel."""

# Needed for python a-sync threads.
import threading
# Needed to run processes and get output.
import subprocess
# Needed to get defined paths.
import path_manager as pm
# Used for basic time measurements.
from universal_code import anything_time_related as atr


ETL_STEP_CREATE_CLEAN_DAM                = 'create_clean_dam'
ETL_STEP_CREATE_CLEAN_EXCEL              = 'create_clean_excel'
ETL_STEP_PARSE_PRODUCTS_FROM_CLEAN_EXCEL = 'parse_products_from_clean_excel'
ETL_STEP_PARSE_DAM_ASSET_INFORMATION     = 'parse_dam_asset_information'
ETL_STEP_MAP_ASSETS_TO_CLEAN_PRODUCTS    = 'map_assets_to_clean_products'
ETL_STEP_CREATE_CSV_AND_DAM_TO_SEND      = 'create_csv_and_dam_to_send'
ETL_STEP_TRANSFER_LOCAL_FILES_TO_AEM     = 'transfer_local_files_to_aem'
ETL_STEP_CREATE_SUB_PACKAGES             = 'create_sub_packages'
ETL_STEP_CALCULATE_STATISTICS            = 'calculate_statistics'

# Only contains the ETL steps that spawn a process.
ALL_ETL_STEPS                            = [ETL_STEP_CREATE_CLEAN_DAM,
                                            ETL_STEP_CREATE_CLEAN_EXCEL]


class ElectricalETLStep(threading.Thread):
	"""A single step in the ETL step, broken down to better enable con-currency."""

	def __init__(self, etl_step_name):
		self.etl_step_name = etl_step_name
		self.stdout        = None
		self.stderr        = None
		self.run_duration  = None
		self.finished      = False
		self.failed        = False
		self.timer         = None
		self.is_a_process  = False
		threading.Thread.__init__(self)

	def _start_timer(self):
		self.timer = atr.EasyTimer()

	def _stop_timer(self):
		self.run_duration = self.timer.get_run_time()
		self.finished     = True

	def run(self):
		"""This threads runs a subprocess.
		:return: The stdout and stderr of that subprocess."""
		self.is_a_process        = True
		self._start_timer()
		p                        = subprocess.Popen(['python3', pm.get_etl_path(self.etl_step_name), self.etl_step_name], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		self.stdout, self.stderr = p.communicate()
		self.stdout              = self.stdout.decode('utf-8')
		self.stderr              = self.stderr.decode('utf-8')
		self._stop_timer()

	def print_step_output(self):
		"""Print the stdout, stderr, and runtime of this ETL step.
		:return: Void."""
		print('ETL Step : ' + self.etl_step_name + ' completed in ' + str(self.run_duration))
		if self.is_a_process:
			if len(self.stderr) != 0:
				print('Errors  : ' + self.stderr)
			if len(self.stdout) != 0:
				print('Output  : ' + self.stdout)
		print('-----------------------------------------------------------------------------')

	def get_output(self):
		"""Return the output of this ETL process, whatever it may be.
		:return: Void."""
		return self.stdout




