# coding=utf-8

"""This module, etl_step_transfer_local_files_to_aem.py, will transfer local files to AEM."""

# Parent class.
import threading

import path_manager as pm
from docker import docker_api
from etl_steps import etl_step


class ElectricalETLStepTransferLocalFilesToAEM(etl_step.ElectricalETLStep):
	"""This ETL step will return a list of Product objects."""

	def __init__(self):
		self.etl_step_name = etl_step.ETL_STEP_TRANSFER_LOCAL_FILES_TO_AEM
		self.docker_api    = docker_api.DockerManager()
		self.author        = self.docker_api.get_docker_instance('author')
		threading.Thread.__init__(self)
		super().__init__(self.etl_step_name)

	def run(self):
		"""Run this ETL step."""
		self._start_timer()
		self.author.copy_local_files_to_this_instance(pm.CSV_FOR_AEM_PATH, pm.DOCKER_AEM_PATH + 'csv_for_aem.csv')
		self.author.copy_local_files_to_this_instance(pm.DAM_FOR_AEM_COMPRESSED_DIRECTORY, pm.DOCKER_AEM_PATH)
		self._stop_timer()

