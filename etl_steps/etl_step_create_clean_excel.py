# coding=utf-8

"""This module, etl_step_create_clean_excel.py, will create a clean Excel file to work with."""

# Needed for working with Excel files.
import openpyxl
# Needed for getting useful paths.
import path_manager as pm
# Used for Excel file IO operations.
from csv_utilities import excel_utilities
# Used to get AEM compliant names.
import aem_api


workbook = openpyxl.load_workbook(pm.EXCEL_ORIGINAL_PATH)

for sheet_name in workbook.get_sheet_names():
	if sheet_name != 'all images':
		worksheet = excel_utilities.WorksheetGeneric(sheet_name, workbook[sheet_name])
		for j, row in enumerate(worksheet.rows):
			for i, element in enumerate(row):
				cell       = worksheet.worksheet.cell(row = j + 1, column = i + 1)
				cell.value = aem_api.get_aem_dam_compliant_version_of_provided_string(cell.value)
	else:
		# Not doing this for now as it introduces Excel bugs.
		#workbook.remove_sheet(workbook.get_sheet_by_name(sheet_name))
		y = 2


workbook.save(pm.EXCEL_CLEANED_PATH)
workbook.close()
