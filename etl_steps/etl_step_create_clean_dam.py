# coding=utf-8

"""This module, etl_step_create_clean_dam.py, will create a clean dam."""

# Used for system operations.
import os
# Used for recursively searching through a directory.
import glob
# Used for getting needed paths.
import path_manager as pm
# Used for AEM name compliance.
from aem import aem_api
# Used for creating directories.
import pathlib
# Used for copying files.
import shutil


def _get_clean_directory_name(original_directory_name):
	"""A utility function to get a clean directory name from the provided directory name,
	:param original_directory_name: The directory name to get a clean version from.
	:return: The cleaned directory name as a string."""
	path = original_directory_name.replace(' ', '_').replace(pm.DAM_ORIGINAL_DIRECTORY, '')
	return pm.DAM_CLEANED_DIRECTORY + aem_api.get_aem_dam_compliant_version_of_provided_string(path, ['/'])

file_names      = []
directory_names = []

for e in glob.glob(pm.DAM_ORIGINAL_DIRECTORY + '/**', recursive=True):
	if os.path.isdir(e):
		directory_names.append(e)
	else:
		file_names.append(e)

# Make the required directories for the cleaned dam.
for d in directory_names:
	# Ignore empty directories.
	if len(os.listdir(d)) > 0:
		clean_directory_name = aem_api.get_aem_dam_compliant_version_of_provided_string(d, ['/']).replace('electrical_content_files/dam_original', 'electrical_content_files/dam_clean')
		pathlib.Path(_get_clean_directory_name(d)).mkdir(parents=True, exist_ok=True)

# Make the directory for the generic image.
pathlib.Path(pm.USE_GENERIC_IMAGE_DAM_CLEANED_DIRECTORY).mkdir(parents=True, exist_ok=True)

# Now copy over the asset files from the dam original.
for f in file_names:
	directory_name = f[0:f.rfind('/') + 1]
	directory_name = _get_clean_directory_name(directory_name)

	file_name      = f[f.rfind('/') + 1:]
	file_name      = aem_api.get_aem_dam_compliant_version_of_provided_string(file_name, ['.'])

	shutil.copy2(f, directory_name + file_name)

# Transfer the generic image.
shutil.copy2(pm.USE_GENERIC_IMAGE_SOURCE_PATH, pm.USE_GENERIC_IMAGE_DAM_CLEANED_PATH)
