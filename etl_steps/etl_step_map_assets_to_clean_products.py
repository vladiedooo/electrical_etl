# coding=utf-8

"""This module, etl_step_parse_products_from_clean_excel.py, will read in the Excel data into Python objects."""

# Parent class.
from etl_steps import etl_step
# Used for threading.
import threading
# The object to work with.
from electrical_classes.product_manager import ElectricalProductManager


class ElectricalETLStepMapAssetsToCleanProducts(etl_step.ElectricalETLStep):
	"""This ETL step will return a list of Product objects."""

	def __init__(self, product_manager: ElectricalProductManager):
		self.etl_step_name   = etl_step.ETL_STEP_MAP_ASSETS_TO_CLEAN_PRODUCTS
		self.product_manager = product_manager
		threading.Thread.__init__(self)
		super().__init__(self.etl_step_name)

	def run(self):
		"""Run this ETL step."""
		self._start_timer()
		self.product_manager.clean_the_raw_products()
		self.product_manager.map_assets_to_clean_products()
		self.product_manager.make_sure_primary_image_assets_are_healthy()
		self._stop_timer()

