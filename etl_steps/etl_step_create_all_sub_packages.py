# coding=utf-8

"""This module, etl_step_parse_products_from_clean_excel.py, will read in the Excel data into Python objects."""

# Parent class.
from etl_steps import etl_step
# Used for threading.
import threading
# The object to work with.
from electrical_classes.product_manager import ElectricalProductManager
# Used to make API calls to AEM.
from aem import aem_api
# Used for getting workflow packages for AEM.
from aem import aem_package
# Used for getting paths.
import path_manager as pm


class ElectricalETLCreateAllSubPackages(etl_step.ElectricalETLStep):
	"""This ETL step will return a list of Product objects."""

	def __init__(self, product_manager: ElectricalProductManager):
		self.etl_step_name    = etl_step.ETL_STEP_CREATE_SUB_PACKAGES
		self.product_manager  = product_manager
		self.local_aem        = aem_api.AEMAPI(aem_api.LOCAL)
		self.stage_aem        = aem_api.AEMAPI(aem_api.STAGE)

		self.stop_workflow    = aem_package.PackageEnableWorkflow()
		self.start_workflow   = aem_package.PackageDisableWorkflow()

		threading.Thread.__init__(self)
		super().__init__(self.etl_step_name)

	def run(self):
		self._start_timer()

		# Disable the DAM workflow.
		self.stop_workflow.install_package(self.local_aem.aem)

		path_to_downloaded_packages = '/Users/utarsuno/git_repos/electrical_etl/electrical_content_files/packages/downloaded/'

		print('There are ' + str(6) + ' packages to upload and install!')
		index = 0
		while index < 6:
			package = aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_' + str(index), '1.0', None)
			print('Working on package : ' + str(package))
			package.upload_package(self.local_aem.aem, path_to_downloaded_packages)
			package.install_package(self.local_aem.aem)
			index += 1

		# Enable the DAM workflow.
		self.start_workflow.install_package(self.stage_aem.aem)

		self._stop_timer()

	def run2(self):
		"""Run this ETL step."""
		self._start_timer()

		# Disable the DAM workflow.
		self.stop_workflow.install_package(self.local_aem.aem)

		# Keep a dictionary of relative paths followed by a list of all product assets that fall under that relative path.
		relative_paths_to_product_list = {}
		aem_packages                   = []

		generic_relative_path = '/content/dam/electrical/generic_image_path/'

		for p in self.product_manager.get_clean_products():
			if p.is_clean():
				if p.is_generic():
					if generic_relative_path not in relative_paths_to_product_list:
						relative_paths_to_product_list[generic_relative_path] = []

					relative_paths_to_product_list[generic_relative_path].append(p)
				else:
					relative_path = p.get_primary_image_asset().get_relative_path()
					if relative_path not in relative_paths_to_product_list:
						relative_paths_to_product_list[relative_path] = []

					relative_paths_to_product_list[relative_path].append(p)

		aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_0', '1.0', '/content/dam/electrical/aerospace'))
		aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_1', '1.0', '/content/dam/electrical/datacomm'))
		aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_2', '1.0', '/content/dam/electrical/generic_image_path'))
		aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_3', '1.0', '/content/dam/electrical/testmeasurement'))
		aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_4', '1.0', '/content/dam/electrical/toolssupplies'))
		aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_5', '1.0', '/content/dam/electrical/wiretermination'))

		#sub_package_number = 0
		#for rp in relative_paths_to_product_list:
			# Create a package for each relative path, set the filter to the relative path.
		#	aem_packages.append(aem_package.Package(aem_package.DEFAULT_GROUP_NAME, 'electrical_content_' + str(sub_package_number), '1.0', '/content/dam/electrical/' + rp))
		#	sub_package_number += 1

		print('There are ' + str(len(aem_packages)) + ' packages to create!')
		for p in aem_packages:
			print('Working on package : ' + str(p))
			#p.upload_package(self.local_aem.aem, pm.)
			#p.create_package(self.local_aem.aem)
			#p.build_package(self.local_aem.aem)
			#p.download_package(self.local_aem.aem)
			#p.install_package(self.local_aem.aem)

		# Re-enable the workflow.
		self.start_workflow.install_package(self.local_aem.aem)

		self._stop_timer()

