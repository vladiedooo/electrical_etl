# coding=utf-8

"""This module, etl_step_parse_products_from_clean_excel.py, will read in the Excel data into Python objects."""

# Parent class.
from etl_steps import etl_step
# Used for threading.
import threading
# Used to get useful paths.
import path_manager as pm
# The object to work with.
from electrical_classes.product_manager import ElectricalProductManager
# Used to generate the CSV for AEM file.
from csv_utilities.csv_for_aem_generator import CSVForAEMGenerator
# Used to generate the DAM for AEM.
from dam.dam_for_aem_generator import DAMForAEMGenerator


class ElectricalETLStepCreateCSVAndDAMToSend(etl_step.ElectricalETLStep):
	"""This ETL step will return a list of Product objects."""

	def __init__(self, product_manager: ElectricalProductManager):
		self.etl_step_name    = etl_step.ETL_STEP_CREATE_CSV_AND_DAM_TO_SEND
		self.product_manager  = product_manager
		self.csv_generator    = CSVForAEMGenerator(self.product_manager.get_clean_products(), pm.CSV_FOR_AEM_PATH)
		self.dam_generator    = None
		threading.Thread.__init__(self)
		super().__init__(self.etl_step_name)

	def run(self):
		"""Run this ETL step."""
		self._start_timer()

		# Create the CSV file.
		self.csv_generator.generate_csv_to_send()

		# Create the DAM.
		self.dam_generator = DAMForAEMGenerator(self.csv_generator.get_assets_needed())
		self.dam_generator.generate_dam_to_send()

		self._stop_timer()

