# coding=utf-8

"""This module, excel_utilities.py, contains functionality relating to Excel files."""

# Used to read Excel files.
import openpyxl
# Business objects.
from electrical_classes import product


class WorksheetGeneric:
	"""This objects represents a single worksheet in an Excel file."""

	def __init__(self, sheet_name, worksheet):
		self.sheet_name = sheet_name
		self.worksheet  = worksheet
		self.headers    = []
		self.rows       = []
		for i, row in enumerate(self.worksheet.rows):
			if i == 0:
				for cell in row:
					self.headers.append(str(cell.value).strip().lower())
			else:
				single_row = []
				for cell in row:
					single_row.append(str(cell.value).strip())
				self.rows.append(single_row)


class ElectricalExcelParser:
	"""This objects reads in the data from an Excel file."""

	def __init__(self, path_to_cleaned_excel_file):
		#if not path_manager.does_file_exist_at_(path_manager.PATH_TO_ELECTRICAL_EXCEL_CLEANED):
		#	self.create_cleaned_excel_file()
		self.file_path   = path_to_cleaned_excel_file
		self.workbook    = None
		self.worksheets  = []
		self.skus        = {}

	def parse_out_products(self):
		"""Read the file into program memory.
		:return: Void."""
		self.workbook = openpyxl.load_workbook(self.file_path, read_only=True)
		sheet_names   = self.workbook.get_sheet_names()
		for sn in sheet_names:
			if sn != 'all images':
				self.worksheets.append(WorksheetGeneric(sn, self.workbook[sn]))
		self.workbook.close()

		# Go through each worksheet.
		for work_sheet in self.worksheets:
			# Go through each row.
			for row in work_sheet.rows:
				# Go through each element and generate the properties dictionary.
				properties = {}
				for i, element in enumerate(row):
					if element.strip().lower() != 'none':
						column_name = str(work_sheet.headers[i])
						if i == 0:
							sku                     = element
							properties[column_name] = sku
						else:
							properties[column_name] = element
				if sku not in self.skus:
					self.skus[sku] = product.ElectricalProduct(properties)
				else:
					if len(properties) > 0:
						if properties != self.skus[sku].properties:
							self.skus[sku].add_sku_clone(product.ElectricalProduct(properties))

		return self.skus.values()

