# coding=utf-8

"""This module, csv_for_aem_generator.py, will create the CSV file for AEM."""

# Used to get product property names.
from electrical_classes import product
# Used to help the IDE.
from typing import List

# Electrical Columns.
SKU                  = ['sku', '../metadata/SKU {{ String }}']                                  # 0x00
PRODUCT_REFERENCE    = ['NA', 'cq:productReference {{ String }}']                               # 0x01
PRODUCT_ID           = ['sku', 'dam:productID {{ String }}']                                    # 0x02
PRODUCT_NAME         = ['product_name', 'dc:title {{ String : multi }}']                        # 0x03
RELATIVE_SOURCE_PATH = ['NA', 'relSrcPath']                                                     # 0x04
IMAGE_NAME           = ['primary_image_name', 'imageName {{ String }}']                         # 0x05
ABSOLUTE_TARGET_PATH = ['NA', 'absTargetPath']                                                  # 0x06
PRIMARY_IMAGE_NAME   = ['primary_image_name', '../metadata/primary_image {{ String }}']         # 0x07
SECONDARY_IMAGE      = ['secondary_image', '../metadata/secondary_image {{ String }}']          # 0x08
TERTIARY_IMAGE       = ['tertiary_image', '../metadata/tertiary_image {{ String }}']            # 0x09
QUATERNARY_IMAGE     = ['quaternary_image', '../metadata/quaternary_image {{ String }}']        # 0x10
QUINARY_IMAGE        = ['quinary_image', '../metadata/quinary_image {{ String }}']              # 0x11
PDF_WARRANTY         = ['pdf-warranty', '../metadata/pdf_warranty {{ String }}']                # 0x12
PDF_INSTRUCTIONS     = ['pdf-instructions', '../metadata/pdf_instructions {{ String }}']        # 0x13
PDF_MISCELLANEOUS    = ['pdf-miscellaneous', '../metadata/pdf_miscellaneous {{ String }}']      # 0x14
PDF_MISCELLANEOUS_2  = ['pdf-miscellaneous_2', '../metadata/pdf_miscellaneous_2 {{ String }}']  # 0x15
PDF_MISCELLANEOUS_3  = ['pdf-miscellaneous_3', '../metadata/pdf_miscellaneous_3 {{ String }}']  # 0x16
PDF_SPEC_SHEET       = ['pdf-spec_sheet', '../metadata/pdf_spec_sheet {{ String }}']            # 0x17
PDF_SPEC_SHEET_2     = ['pdf-spec_sheet_2', '../metadata/pdf_spec_sheet_2 {{ String }}']        # 0x18
PDF_SELL_SHEET       = ['pdf-sell_sheet', '../metadata/pdf_sell_sheet {{ String }}']            # 0x19
PDF_SELL_SHEET_2     = ['pdf-sell_sheet2', '../metadata/pdf_sell_sheet_2 {{ String }}']         # 0x20
TAGS                 = ['NA', 'cq:tags {{ String : multi }}']                                   # 0x21
PARENT_CATEGORY      = ['NA', '../metadata/parent_category {{ String }}']                       # 0x22
SUB_CATEGORY         = ['NA', '../metadata/sub_category {{ String }}']                          # 0x23

# CSV Column mapping.
csv_columns = [SKU,
               PRODUCT_REFERENCE,
               PRODUCT_ID,
               PRODUCT_NAME,
               RELATIVE_SOURCE_PATH,
               IMAGE_NAME,
               ABSOLUTE_TARGET_PATH,
               PRIMARY_IMAGE_NAME]


class CSVForAEMGenerator:
	"""Generates the CSV file to send to AEM."""

	def __init__(self, products: List[product.ElectricalProduct], csv_path):
		self.products  = products
		self.csv_path  = csv_path
		self.final_csv = ''
		self.assets_needed = []

	def _add_csv_entry(self, entry):
		self.final_csv += entry + ','

	def _end_csv_row(self):
		self.final_csv = self.final_csv[0:-1] + '\n'

	def generate_csv_to_send(self):
		"""Creates the csv_for_aem.csv file.
		:return: Void."""

		for column in csv_columns:
			self._add_csv_entry(column[1])
		self._end_csv_row()

		for p in self.products:
			self._add_csv_entry(p.get_sku())
			self._add_csv_entry('/etc/commerce/products/electrical/en/buckets/' + p.get_sku()[:2] + '/' + p.get_sku())
			self._add_csv_entry(p.get_sku())
			self._add_csv_entry(p.get_product_name())
			csv_row += p.get_sku() + ',' + p.get_product_name() + ','

			if p.is_generic():
				absolute_target_path = '/content/dam/electrical/generic_image_path/use_generic_image.jpg'
				relative_source_path = 'use_generic_image.jpg'
			else:
				primary_image_asset  = p.get_primary_image_asset()
				absolute_target_path = '/content/dam/electrical/' + primary_image_asset.get_relative_path() + p.get_jcr_node_name()
				relative_source_path = primary_image_asset.get_relative_path() + primary_image_asset.get_file_name()

				self.assets_needed.append(p.get_primary_image())

			'''
			SKU                  = ['sku', '../metadata/SKU {{ String }}']                                  # Covered
			PRODUCT_REFERENCE    = ['NA', 'cq:productReference {{ String }}']                               # Covered
			PRODUCT_ID           = ['sku', 'dam:productID {{ String }}']                                    # Covered
			PRODUCT_NAME         = ['product name', 'dc:title {{ String : multi }}']                        #
			RELATIVE_SOURCE_PATH = ['NA', 'relSrcPath']                                                     #
			IMAGE_NAME           = ['primary image name', 'imageName {{ String }}']                         #
			ABSOLUTE_TARGET_PATH = ['NA', 'absTargetPath']                                                  #
			PRIMARY_IMAGE_NAME   = ['primary image name', '../metadata/primary_image {{ String }}']         #
			SECONDARY_IMAGE      = ['secondary image', '../metadata/secondary_image {{ String }}']          #
			TERTIARY_IMAGE       = ['tertiary image', '../metadata/tertiary_image {{ String }}']            #
			QUATERNARY_IMAGE     = ['quaternary image', '../metadata/quaternary_image {{ String }}']        #
			QUINARY_IMAGE        = ['quinary image', '../metadata/quinary_image {{ String }}']              #
			PDF_WARRANTY         = ['pdf-warranty', '../metadata/pdf_warranty {{ String }}']                #
			PDF_INSTRUCTIONS     = ['pdf-instructions', '../metadata/pdf_instructions {{ String }}']        #
			PDF_MISCELLANEOUS    = ['pdf-miscellaneous', '../metadata/pdf_miscellaneous {{ String }}']      #
			PDF_MISCELLANEOUS_2  = ['pdf-miscellaneous 2', '../metadata/pdf_miscellaneous_2 {{ String }}']  #
			PDF_MISCELLANEOUS_3  = ['pdf-miscellaneous 3', '../metadata/pdf_miscellaneous_3 {{ String }}']  #
			PDF_SPEC_SHEET       = ['pdf-spec sheet', '../metadata/pdf_spec_sheet {{ String }}']            #
			PDF_SPEC_SHEET_2     = ['pdf-spec sheet 2', '../metadata/pdf_spec_sheet_2 {{ String }}']        #
			PDF_SELL_SHEET       = ['pdf-sell sheet', '../metadata/pdf_sell_sheet {{ String }}']            #
			PDF_SELL_SHEET_2     = ['pdf-sell sheet2', '../metadata/pdf_sell_sheet_2 {{ String }}']         #
			TAGS                 = ['NA', 'cq:tags {{ String : multi }}']                                   #
			PARENT_CATEGORY      = ['NA', '../metadata/parent_category {{ String }}']                       #
			SUB_CATEGORY         = ['NA', '../metadata/sub_category {{ String }}']                          #
			'''

			csv_row += relative_source_path + ',' + p.get_primary_image() + ',' + absolute_target_path + ',' + p.get_primary_image() + '\n'
			self.final_csv += csv_row

		# Now create the CSV file.
		with open(self.csv_path, 'w+', ) as file_handler:
			for line in self.final_csv.split('\n'):
				file_handler.write(line + '\n')

	def get_assets_needed(self):
		"""Returns a list of the assets needed.
		:return: A list of ElectricalAsset objects."""
		return self.assets_needed




