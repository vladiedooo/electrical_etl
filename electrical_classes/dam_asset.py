# coding=utf-8

"""This module, dam_asset.py, represents an Electrical DAM asset by unique product_name + path."""

# Does simple lexical analysis.
import shlex
# Needed for launching sub-processes.
import subprocess
# Used for marking various types of asset errors.
from electrical_classes import asset_error as ae


class ElectricalAsset:
	"""Each object instance maps one to one with an asset file."""

	MIME_TYPE_TO_FILE_ENDING = {'image/jpeg': 'jpg', 'image/tiff': 'tif'}

	def __init__(self, file_path):
		self.file_path = file_path
		self.file_name = self.file_path[self.file_path.rfind('/') + 1:]
		if '.' in self.file_name:
			self.file_ending = self.file_name[self.file_name.rfind('.') + 1:]
		else:
			self.file_ending = None
			#self.set_error(ae.AssetErrorInvalidFileEnding)
		self.relative_directory_path = self.file_path[self.file_path.rfind('electrical_content_files/dam_cleaned') + len('electrical_content_files/dam_cleaned') + 1:].replace(self.file_name, '').replace(' ', '_')
		self.mime_type  = None
		self.width      = None
		self.height     = None
		self.error      = None

	def get_file_type_from_mime_type(self):
		"""Returns the file type string based off this asset's mime type.
		:return: A string representing the file mime type."""
		if self.mime_type in ElectricalAsset.MIME_TYPE_TO_FILE_ENDING:
			return ElectricalAsset.MIME_TYPE_TO_FILE_ENDING[self.mime_type]

	def is_clean(self):
		"""Indicates if this asset is clean or not.
		:return: A boolean indicating clean status."""
		if self.error is not None:
			return False
		return True

	def get_file_ending(self) -> str:
		"""Returns the file ending of this asset.
		:return: The file ending of this asset as a string."""
		return self.file_ending

	def get_file_name(self) -> str:
		"""Gets the file name of this asset.
		:return: The file name as a string."""
		return self.file_name

	def get_relative_path(self) -> str:
		"""Returns the relative path to this asset.
		:return: The relative path as a string."""
		return self.relative_directory_path

	def get_full_path(self) -> str:
		"""Returns the full path to this asset.
		:return: The full path as a string."""
		return self.file_path

	def run_health_checks(self):
		"""This will run health checks on this file.
		:return: Void."""
		self.mime_type = self._get_mime_type()

		# TODO : Fix these errors later. These assets will be marked as bad for now but once Electrical is uploaded then work can go back to this.
		if str(self.file_ending) != 'None':
			if str(self.mime_type) != 'image/jpeg':
				self.set_error(ae.AssetErrorInvalidMimeType)
		else:
			self.set_error(ae.AssetErrorInvalidFileEnding)

	def _get_mime_type(self):
		cmd       = shlex.split('file --mime-type {0}'.format('"' + self.file_path + '"'))
		result    = subprocess.check_output(cmd)
		mime_type = result.split()[-1]
		return mime_type.decode('ascii')

	def throw_error_if_over_one_million_pixels(self):
		"""This will get the files width and height or catch any errors from 'un-predictable' errors.'
		:return: Void."""
		try:
			result = subprocess.check_output(['file', self.file_path]).decode('utf-8')

			width  = ''
			height = ''
			x_position = result.rfind('x')

			index   = 1
			go_back = True
			while go_back:
				width = str(result[x_position - index]) + str(width)
				index += 1
				if result[x_position - index] == ' ':
					go_back = False

			index      = 1
			go_forward = True
			while go_forward:
				height += result[x_position + index]
				index += 1
				if result[x_position + index] == ' ':
					go_forward = False
			height = height[0:-1]

			self.width  = int(width)
			self.height = int(height)

			if self.width * self.height > 1000000:
				self.set_error(ae.AssetErrorImageFileTooBig)
		except Exception as e:
			self.set_error(ae.AssetErrorFileCommandFailed)

	def __str__(self):
		return self.file_name + '{' + str(self.mime_type) + '} - ' + self.relative_directory_path

	# Adds an asset error.
	def set_error(self, error_class_type):
		"""Sets this product to not be clean.
		:param error_class_type : The error type that occured. Provided as a class.
		:return: Void."""
		self.error = error_class_type()
