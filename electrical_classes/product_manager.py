# coding=utf-8

"""This module, product_manager.py, will manage the products."""

# Used for IDE.
from typing import List
# Used to represent Electrical products.
from electrical_classes import product
# Used to represent Electrical assets.
from electrical_classes.dam_asset import ElectricalAsset
# Used for marking products as not clean from various error types.
from electrical_classes import product_error as pe
# Used to get AEM valid strings.
from aem import aem_api


class ElectricalProductManager:
	"""Manages a list of Electrical products."""

	def __init__(self, products: List[product.ElectricalProduct], assets: List[ElectricalAsset]):
		self.products = products
		self.assets   = assets

	def get_clean_products(self):
		"""Returns a list of only clean products.
		:return: A list of Electrical product objects that are clean."""
		clean_products = []
		for p in self.products:
			if p.is_clean():
				clean_products.append(p)
		return clean_products

	def clean_the_raw_products(self):
		"""Marks products as non-clean as needed.
		:return: Void."""

		print('Number of SKUs currently being worked with : ' + str(len(self.get_clean_products())))

		for p in self.products:
			if p.is_clean():
				if p.has_property(product.PRIMARY_IMAGE_NAME):
					if p.get_primary_image() == product.USE_GENERIC_IMAGE:
						p.uses_generic_image = True
					else:
						if p.get_primary_image() in product.BAD_PRIMARY_IMAGE_NAME_VALUES:
							p.set_error(pe.ProductErrorBadPrimaryImageValue)
				else:
					p.set_error(pe.ProductErrorNoPrimaryImage)

		for p in self.products:
			if p.is_clean():
				if p.has_property(product.PRODUCT_NAME):
					if p.get_product_name().lower() == 'none':
						p.set_error(pe.ProductErrorInvalidProductName)
				else:
					p.set_error(pe.ProductErrorNoProductNameKey)

	def map_assets_to_clean_products(self):
		"""Maps assets to clean products for easier access.
		:return: Void."""

		print('Number of SKUs currently being worked with : ' + str(len(self.get_clean_products())))

		for p in self.products:
			if p.is_clean() and not p.is_generic():
				primary_image_name = p.get_primary_image()
				asset_match        = self._is_file_name_in_asset_names(primary_image_name)
				if asset_match is not None:
					p.add_primary_image_asset(asset_match)
				else:
					p.set_error(pe.ProductErrorNoPrimaryImageAssetFound)


		# Map the other attributes here. Don't fail if the attributes don't exist though.
		#for p in self.products:
		#	if p.is_clean() and not p.is_generic():



	def make_sure_primary_image_assets_are_healthy(self):
		"""Checks the primary image assets for any issues.
		:return: Void."""

		print('Number of SKUs currently being worked with : ' + str(len(self.get_clean_products())))

		for p in self.products:
			if p.is_clean() and not p.is_generic():
				p.get_primary_image_asset().run_health_checks()
				if not p.get_primary_image_asset().is_clean():
					p.set_error(pe.ProductErrorAssetFileHasError)

		print('Number of SKUs currently being worked with : ' + str(len(self.get_clean_products())))

		#for p in self.products:
		#	if p.is_clean() and not p.is_generic():
		#		p.get_primary_image_asset().throw_error_if_over_one_million_pixels()
		#
		#		if not p.get_primary_image_asset().is_clean():
		#			p.set_error(pe.ProductErrorAssetFileHasError)

		print('Number of SKUs currently being worked with : ' + str(len(self.get_clean_products())))

	# TODO : Map out all the assets to products!!! Not just primary image value.
	# Note : This will be done once a successful upload of assets in completed.

	def _is_file_name_in_asset_names(self, file_name):

		for a in self.assets:

			# TODO : Polish this section

			if file_name == a.file_name.replace('.', '_') or aem_api.get_aem_dam_compliant_version_of_provided_string(file_name) == a.file_name:
				return a



		return None




