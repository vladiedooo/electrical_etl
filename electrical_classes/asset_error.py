# coding=utf-8

"""This module, product_error.py, contains various errors products can have."""


class AssetError:
	"""A single instance maps to a single non-clean asset."""

	def __init__(self, error_type):
		self.error_type = error_type


class AssetErrorInvalidMimeType(AssetError):
	"""Indicates invalid mime-type."""

	def __init__(self):
		super().__init__('Asset file has invalid MIME Type!')


class AssetErrorInvalidFileEnding(AssetError):
	"""Invalid invalid file extension (or lack of)."""

	def __init__(self):
		super().__init__('Asset file has invalid extension!')


class AssetErrorFileCommandFailed(AssetError):
	"""If the bash 'file <file_path>' fails then make a note of this."""

	def __init__(self):
		super().__init__('Bash command \'file <file_path>\' failed on this asset!')


class AssetErrorImageFileTooBig(AssetError):
	"""Temporary error : if the image file is over one million pixels."""

	def __init__(self):
		super().__init__('Image file is over one million pixels.')

