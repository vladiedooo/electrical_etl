# coding=utf-8

"""This module, product.py, represents an Electrical product by unique SKU."""

# Used to mark products as not clean from various error sources.
from electrical_classes import product_error as pe
# Used to represent the assets that this product is linked to.
from electrical_classes import dam_asset

# Product properties.
SKU                  = 'sku'
PRODUCT_NAME         = 'product_name'
IMAGE_NAME           = 'image_name'
PRIMARY_IMAGE_NAME   = 'primary_image_name'
SECONDARY_IMAGE      = 'secondary_image'
TERTIARY_IMAGE       = 'tertiary_image'
QUATERNARY_IMAGE     = 'quaternary_image'
QUINARY_IMAGE        = 'quinary_image'
PDF_WARRANTY         = 'pdf-warranty'
PDF_INSTRUCTIONS     = 'pdf-instructions'
PDF_MISCELLANEOUS    = 'pdf-miscellaneous'
PDF_MISCELLANEOUS_2  = 'pdf-miscellaneous_2'
PDF_MISCELLANEOUS_3  = 'pdf-miscellaneous_3'
PDF_SPEC_SHEET       = 'pdf-spec_sheet'
PDF_SPEC_SHEET_2     = 'pdf-spec_sheet_2'
PDF_SELL_SHEET       = 'pdf-sell_sheet'
PDF_SELL_SHEET_2     = 'pdf-sell_sheet2'

# Utility variables for values that show up in the Excel file.
USE_GENERIC_IMAGE  = 'use_generic_image'
BAD_PRIMARY_IMAGE_NAME_VALUES = ['discontinued_', 'should_these_be_listed_']


class ElectricalProduct:
	"""Each object instance maps one to one with a unique SKU."""

	def __init__(self, properties):
		self.properties                       = properties
		self.assets                           = []
		self.asset_that_maps_to_primary_image = None
		self.uses_generic_image               = False
		self._node_name                       = None
		self.error                            = None

		# Misc utility properties.
		self.sku_clones = []

	def get_jcr_node_name(self):
		"""Gets the JCR node name of this product.
		:return: The JCR node name of this product as a string."""
		if self._node_name is None:
			if self.is_generic():
				self._node_name = self.get_sku() + self.get_product_name() + '.jpg'
			else:
				self._node_name = self.get_sku() + self.get_product_name() + '.' + self.get_primary_image_asset().get_file_type_from_mime_type()
		return self._node_name

	def get_primary_image_asset(self) -> dam_asset.ElectricalAsset:
		"""Returns the primary image asset of this product.
		:return: An Electrical asset object.
		"""
		return self.asset_that_maps_to_primary_image

	def add_primary_image_asset(self, asset):
		"""Add the asset that maps to the primary image.
		:param asset : The asset that maps to the primary image.
		:return: Void."""
		self.asset_that_maps_to_primary_image = asset

	def add_asset(self, asset):
		"""Add an asset to this product.
		:param asset : The asset to add.
		:return: Void."""
		self.assets.append(asset)

	def is_generic(self):
		"""Indicates if this SKU maps to a generic image or not.
		:return: A boolean indicating generic status."""
		return self.uses_generic_image

	def is_clean(self):
		"""Indicates if this SKU is clean or not.
		:return: A boolean indicating clean status."""
		if self.error is not None:
			return False
		return True

	# Adds a product error.
	def add_sku_clone(self, electrical_product_clone):
		"""Not an actual clone but a duplicate SKU requires both instances to be checked.
		:param electrical_product_clone: The ElectricalProduct object to keep track of for later error checks.
		:return: Void."""
		self.sku_clones.append(electrical_product_clone)
		if self.is_clean:
			self.error = pe.ProductErrorDuplicateSKU()

	def has_property(self, property):
		"""Checks if this product has a property.
		:param property : The property to check for.
		:return: Void."""
		if property in self.properties:
			return True
		return False

	# Adds a product error.
	def set_error(self, error_class_type):
		"""Sets this product to not be clean.
		:param error_class_type : The error type that occured. Provided as a class.
		:return: Void."""
		self.error = error_class_type()

	def get_primary_image(self) -> str:
		"""Returns the primary image value.
		:return: The primary image value as a string."""
		if self.get_primary_image_asset() is not None:
			return self.get_primary_image_asset().get_file_name()
		return self.properties[PRIMARY_IMAGE_NAME]

	# Older code below.

	def get_sub_properties_dictionary_for_keys_that_require_asset_file_matching(self):
		"""Returns a new properties dictionary that will have the same key and values except will only include keys that require its value to be matched as an actual asset file image.
		:return: The sub dictionary."""
		sub_properties = {}
		for k in self.properties:
			if k != 'sku' and k != 'original image name' and k != 'product name' and str(self.properties[k]).lower().strip() != 'use generic image':
				sub_properties[k] = self.properties[k]
		return sub_properties

	def get_sku(self) -> str:
		"""Return the unique SKU of this product.
		:return: The unique SKU as a string."""
		return self.properties['sku']

	def get_product_name(self) -> str:
		"""Get the name of this product.
		:return: The name of the product as a string."""
		return self.properties['product_name']

	def __str__(self):
		if 'sku' in self.properties:
			return 'SKU{' + self.get_sku() + '} : ' + str(self.properties)
		return 'SKU{MISSING}'

