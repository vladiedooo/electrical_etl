# coding=utf-8

"""This module, product_error.py, contains various errors products can have."""


class ProductError:
	"""A single instance maps to a single non-clean SKU."""

	def __init__(self, error_type):
		self.error_type = error_type


class ProductErrorDuplicateSKU(ProductError):
	"""Indicates that the Excel file provided a duplicate SKU row without the same values."""

	def __init__(self):
		super().__init__('Duplicate SKU row in Excel without values matching!')


class ProductErrorNoPrimaryImage(ProductError):
	"""Indicates that the Excel file had no value for primary image for this product."""

	def __init__(self):
		super().__init__('SKU did not have a primary image!')


class ProductErrorBadPrimaryImageValue(ProductError):
	"""Indicates that the Excel file had a bad primary image value such as ('should these be listed?') for this product."""

	def __init__(self):
		super().__init__('SKU did not have a valid primary image value!')


class ProductErrorNoPrimaryImageAssetFound(ProductError):
	"""Indicates that the primary image value was not found as an asset in the dam provided."""

	def __init__(self):
		super().__init__('SKU did not have a primary image that was found as an asset in the dam!')


class ProductErrorAssetFileHasError(ProductError):
	"""Indicates that that an asset file mapped to the product has an error."""

	def __init__(self):
		super().__init__('Asset file mapped to this SKU has an error!')


class ProductErrorInvalidProductName(ProductError):
	"""Indicates that the product does not have a valid product name value."""

	def __init__(self):
		super().__init__('Indicates that the product does not have a valid product name value.')


class ProductErrorNoProductNameKey(ProductError):
	"""Indicates that the product does not have a product name key."""

	def __init__(self):
		super().__init__('Indicates that the product does not have a product name key.')

