# coding=utf-8

"""This module, debugging.py, will contain code related to debugging."""


class MyException(Exception):
	"""Just something useful to have to throw some of my own custom exception."""
	pass


class ParameterException(Exception):
	"""A custom exception for when a function receives bad parameter data."""
	def __init__(self, message):
		super(ParameterException, self).__init__(message)


class AbstractMethodNotImplementedException(Exception):
	"""A custom exception for when a function gets called that hasn't been set in a child class."""
	def __init(self, message):
		super(AbstractMethodNotImplementedException, self).__init__(message)


def raise_exception(exception, message):
	raise exception(message)

# Terminal font coloring and styling.

class TextColors:
	HEADER    = '\033[95m'
	OK_BLUE   = '\033[94m'
	OK_GREEN  = '\033[92m'
	WARNING   = '\033[93m'
	FAIL      = '\033[91m'
	ENDC      = '\033[0m'
	BOLD      = '\033[1m'
	UNDERLINE = '\033[4m'

def print_text_with_color(text, color, end=None):
	if end is None:
		print(color + text + TextColors.ENDC + '\n')
	else:
		print(color + text + TextColors.ENDC, end='')

def terminate(termination_message=''):
	if termination_message is '':
		print_text_with_color('Program termination has been initiated, good bye!', TextColors.FAIL)
	else:
		print_text_with_color(termination_message, TextColors.WARNING, '')
		if not termination_message.endswith('.'):
			print_text_with_color('. The program will now terminate.', TextColors.FAIL)
		else:
			print_text_with_color(' The program will now terminate.', TextColors.FAIL)
	exit()
