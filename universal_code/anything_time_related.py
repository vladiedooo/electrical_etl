# -*- coding: utf-8 -*-

"""
This module, anything_time_related, just contains utility functions and classes for anything time related.

last generated : 2017-03-13 12:56:29

Base File Layout:
------------------------------------------------------------------------------------------
| 0x0 | Libraries                 | All code needed for 3rd party code setup.            |
| 0x1 | Global resources          | All code and variables that are accessed globally.   |
| 0x2 | Classes                   | Most classes will be defined here.                   |
| 0x3 | Code Content              | The actual logic of this code file.                  |
------------------------------------------------------------------------------------------
"""
# 0x0: -----------------------------------------Libraries----------------------------------------------
# Needed for time operations such as time stamps.
import time
# 0x1: -------------------------------------Global Resources-------------------------------------------

# 0x2: ---------------------------------------Most Classes---------------------------------------------


class EasyTimer:
	"""A timer that starts once the object is initialized and prints elapsed time on str() method. There is no stop on this clock."""

	def __init__(self) -> None:
		"""The constructor.
		start_time | The start time of this clock. It will be during object creation.
		"""
		self._start_time          = time.time()
		self._manually_stopped    = None
		self._run_time            = None
		self._last_time_increment = None

	def _get_formatted_time(self):
		return str('{0:2f}'.format(time.time() - self._start_time)) + 's'

	def _get_formatted_time_increment(self):
		temp = self._last_time_increment
		self._last_time_increment = time.time()
		return str('{0:2f}'.format(time.time() - temp)) + 's'

	def stop(self):
		"""This stops the timer. If this function is not called the timer will automatically stop on output printing.
		:return:"""
		self._run_time         = self._get_formatted_time()
		self._manually_stopped = 'elapsed time : ' + self._run_time

	def get_run_time(self):
		"""This returns the timers run duration. If the stop function has not yet been called then this function will call it first.
		:return: The total run time of this clock as a float."""
		if self._manually_stopped is None:
			self.stop()
		return self._run_time

	def get_incremental_time(self):
		"""This returns the current total duration plus the time delta from the last call to 'get_incremental_time'.
		:return: A string."""
		if self._last_time_increment is None:
			self._last_time_increment = time.time()
			return 'elapsed time : ' + self._get_formatted_time()
		else:
			return 'elapsed time : ' + self._get_formatted_time() + ', delta time : ' + self._get_formatted_time_increment()

	def __str__(self) -> str:
		if self._manually_stopped is None:
			return 'elapsed time : ' + str('{0:2f}'.format(time.time() - self._start_time)) + 's'
		return self._manually_stopped


# 0x3: ---------------------------------------Code Content---------------------------------------------
# 0xE: ----------------------------------------End of File---------------------------------------------
