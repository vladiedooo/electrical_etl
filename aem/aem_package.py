# coding=utf-8

"""This module, aem_package.py, will define properties of AEM packages."""

# Used for AEM API functionality.
import pyaem
# Used to get the paths of pre-made packages.
import path_manager as pm
# Used for debugging purposes.
from universal_code import debugging as dbg

DEFAULT_GROUP_NAME = 'my_packages'


class Package:
	"""Represents an AEM package."""

	def __init__(self, group_name, package_name, version, path_filter, file_path=None):
		self.package_name = package_name
		self.group_name   = group_name
		self.version      = version
		self.path_filter  = path_filter
		self.file_path    = file_path

	# TODO : Make sure all methods have error catching.

	def upload_package(self, aem, file_path_to_use=None):
		"""Upload the package to AEM.
		:param aem : The object used to make API calls to AEM.
		:param file_path_to_use : The file path to use if one is not defined already.
		:return: Void."""
		if self.file_path is None and file_path_to_use is None:
			dbg.terminate('Can\'t upload a package without the file path to the package defined!')

		if file_path_to_use is None:
			aem.upload_package(self.group_name, self.package_name, self.version, self.file_path)
		else:
			aem.upload_package(self.group_name, self.package_name, self.version, file_path_to_use)

	def build_package(self, aem):
		"""Build this package.
		:param aem : The object used to make API calls to AEM.
		:return: Void."""
		aem.build_package(self.group_name, self.package_name, self.version)

	def download_package(self, aem):
		"""Downloads this package.
		:param aem : The object used to make API calls to AEM.
		:return: Void."""
		try:
			aem.download_package(self.group_name, self.package_name, self.version, pm.DOWNLOADED_PACKAGES_DIRECTORY)
		except Exception as e:
			print('ERROR DOWNLOADING {' + str(e) + '}')

	def install_package(self, aem):
		"""Install this package.
		:param aem : The object used to make API calls to AEM.
		:return: """
		try:
			result = aem.install_package(self.group_name, self.package_name, self.version)
			if not result.is_success():
				print('Error with installing the package!')
		except Exception as e:
			print('Error with installing the package! {' + str(e) + '}')

	def create_package(self, aem):
		"""Creates the specified package.
		:param aem : The object used to make API calls to AEM.
		:return: Void."""
		try:
			result = aem.create_package(self.group_name, self.package_name, self.version)
			result = aem.update_package_with_filter(self.group_name, self.package_name, self.version, self.path_filter)

			if result.is_success():
				return True
			else:
				print('Error occurred! Printing out debugging information :')
				print(result.response['http_code'])
				print(result.response['body'])
				print(result.response['request']['method'])
				print(result.response['request']['url'])
				print(result.response['request']['params]'])
				print(result.debug())
				return False
		except pyaem.PyAemException as e:
			print('Error occurred! Printing out debugging information :')
			print(e.message)
			print(e.code)
			print(e.response['http_code'])
			print(e.response['body'])
			print(e.response['request']['method'])
			print(e.response['request']['url'])
			print(e.response['request']['params]'])
			return False

	def __str__(self):
		return self.package_name

# Pre-defined packages below.


class PackageEnableWorkflow(Package):
	"""A package used for enabling specific workflows."""

	def __init__(self):
		super().__init__('disable_dam_workflow', DEFAULT_GROUP_NAME, '1.0', '/etc/workflow/models/dam/update_asset', pm.PACKAGE_WORKFLOW_ORIGINAL)


class PackageDisableWorkflow(Package):
	"""A package used for disabling specific workflows."""

	def __init__(self):
		super().__init__('enable_dam_workflow', DEFAULT_GROUP_NAME, '1.0', '/etc/workflow/models/dam/update_asset', pm.PACKAGE_WORKFLOW_EMPTY)




