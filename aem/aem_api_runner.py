# coding=utf-8

"""This module, aem_api_runner.py, will be used to run API calls against AEM."""

from aem import aem_api

aem_api = aem_api.AEMAPI(aem_api.LOCAL)


