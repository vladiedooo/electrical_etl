# coding=utf-8

"""This module, aem_api.py, provides utilities relating AEM and a small API to work to interact with."""

# Used for getting useful paths and urls.
import path_manager as pm
# Used for making Basic Authorizations in HTTP.
from requests.auth import HTTPBasicAuth
# Used to throw exceptions.
from universal_code import debugging as dbg
# Used for making simplified HTTP calls.
import requests
# Used to perform AEM operations.
import pyaem


from urllib.parse import urlencode, quote_plus


# Useful AEM links.
'''
LOCATION OF DAM FILES ON THE LOCAL AEM INSTANCE :
/aem/crx-quickstart/bin/dam/electrical/
_________________________________________________________________________________
WORKFLOWS :
http://localhost:4502/libs/cq/workflow/admin/console/content/models.html/etc/workflow/models

https://author-ideal-industries-stage1.adobecqms.net/libs/cq/workflow/admin/console/content/models.html/etc/workflow/models
_________________________________________________________________________________
WORKFLOW LAUNCHERS :
http://localhost:4502/libs/cq/workflow/admin/console/content/launchers.html

https://author-ideal-industries-stage1.adobecqms.net/libs/cq/workflow/admin/console/content/launchers.html
_________________________________________________________________________________
MISC ADMIN :
http://localhost:4502/miscadmin
_________________________________________________________________________________
'''

# JCR local name - and valid XML characters except {'/', ':', '[', ']', '|', '*'}


def is_string_aem_dam_compliant(string):
	"""Checks if the provided string is AEM compliant.
	:param string: The string to check for AEM compliance.
	:return: Void."""
	for c in str(string).strip().lower():
		if c != '-' and c != '_' and not c.isdigit() and not c.isalpha():
			return False
	return True


def get_aem_dam_compliant_version_of_provided_string(string, characters_to_ignore=None):
	"""The string to convert into an AEM compliant version.
	:param string               : The string to make AEM compliant.
	:param characters_to_ignore : The characters to ignore in the original string.
	:return: Void."""
	if characters_to_ignore is None:
		characters_to_ignore = []
	compliant_version = ''
	for c in str(string).strip().lower():
		if c != '-' and c != '_' and not c.isdigit() and not c.isalpha():
			if c not in characters_to_ignore:
				compliant_version += '_'
			else:
				compliant_version += c
		else:
			compliant_version += c
	return compliant_version


GET_RUNNING_WORKFLOWS_URL = '/etc/workflow/instances.RUNNING.json'

LOCAL                = 'local'
STAGE                = 'stage'

# Original is vtarsunou@sccadv.com
# New is      vtarsunou%40sccadv%2Ecom

# Just doing testing here : https://vtarsunou%40sccadv%2Ecom:vtarsunou%40sccadv%2Ecom@author-ideal-industries-stage1.adobecqms.net:80
# another test            : https://vtarsunou%40sccadv%2Ecom:vtarsunou%40sccadv%2Ecom@author-ideal-industries-stage1.adobecqms.net:443

# Stage will not working because URL authentication is not allowed.

ENVIRONMENTS         = {LOCAL : ['http://localhost:4502', 'admin'],
	                    STAGE : ['https://author-ideal-industries-stage1.adobecqms.net', 'vtarsunou%40sccadv.com']}

PACKAGE_MANAGER_URL         = '/crx/packmgr/index.jsp'
CSV_ASSET_IMPORTER_POST_URL = '/csv-asset-importer.html'


class AEMAPI:
	"""An API to access back-end API calls for AEM."""

	def __init__(self, environment):
		if environment not in ENVIRONMENTS:
			dbg.raise_exception(dbg.ParameterException, 'The passed in environment{' + str(environment) + '} is not valid.')
		self.environment = environment

		c = ENVIRONMENTS[self.environment][1]

		# 3rd party library authentication to AEM.
		if environment is LOCAL:
			self.aem = pyaem.PyAem(c, c, 'localhost', '4502')
		elif environment is STAGE:
			self.aem = pyaem.PyAem(c, c, 'author-ideal-industries-stage1.adobecqms.net', '443')

		# Used for HTTP requests.
		self.auth = HTTPBasicAuth(c, c)

	def _get_environment_url(self):
		return ENVIRONMENTS[self.environment][0]

	def _get_response(self, url):
		response = requests.get(self._get_environment_url() + url, auth=self.auth)
		if response.status_code == 200:
			return response.content.decode('utf-8')
		else:
			print('AEM response failed!')
			print(response.status_code)
			print(response.content)

	def get_running_workflows(self):
		"""Returns all currently running workflows of this environment.
		:return: A list of active workflows as strings."""
		print(self._get_response(GET_RUNNING_WORKFLOWS_URL))

