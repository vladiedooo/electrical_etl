# coding=utf-8

"""This module, electrical_etl.py, will run all the electrical ETL module steps."""

# Objects used to control Electrical ETL steps.
from etl_steps import etl_step
from etl_steps.etl_step_parse_products_from_clean_excel import ElectricalETLStepParseProductsFromCleanExcel as ETL_ParseProducts
from etl_steps.etl_step_parse_dam_asset_information import ElectricalETLStepParseDAMAssetInformation as ETL_ParseDAMAssets
from etl_steps.etl_step_map_assets_to_clean_products import ElectricalETLStepMapAssetsToCleanProducts as ETL_MapAssetsToCleanProducts
from etl_steps.etl_step_create_csv_and_dam_to_send import ElectricalETLStepCreateCSVAndDAMToSend as ETL_CreateCSVAndDAM
from etl_steps.etl_step_transfer_local_files_to_aem import ElectricalETLStepTransferLocalFilesToAEM as ETL_TransferLocalFilesToAEM
from etl_steps.etl_step_create_all_sub_packages import ElectricalETLCreateAllSubPackages as ETL_CreateSubPackages
# Used to measure program length.
from universal_code import anything_time_related as atr
# Used to run ETL assistant operations.
from electrical_classes import product_manager


class ElectricalETL:
	"""Runs the Electrical ETL steps."""

	def __init__(self):
		# Pre-ETL steps.
		self.etl_step_create_clean_dam             = etl_step.ElectricalETLStep(etl_step.ETL_STEP_CREATE_CLEAN_DAM)
		self.etl_step_create_clean_excel           = etl_step.ElectricalETLStep(etl_step.ETL_STEP_CREATE_CLEAN_EXCEL)

		# ETL steps.
		self.etl_step_parse_products               = ETL_ParseProducts()
		self.etl_step_parse_assets                 = ETL_ParseDAMAssets()
		self.etl_step_map_assets_to_clean_products = None
		self.etl_step_create_csv_and_dam_to_send   = None

		# Post-ETL steps.
		self.etl_step_transfer_files_to_aem        = None
		self.etl_step_create_sub_packages          = None

		# Objects that run the show.
		self.product_manager                       = None

	def run_pre_etl_process(self):
		"""This is triggered manually as it does not need to be run every ETL run.
		:return: Void."""
		self.etl_step_create_clean_dam.start()
		self.etl_step_create_clean_excel.start()

		self.etl_step_create_clean_dam.join()
		self.etl_step_create_clean_excel.join()

		self.etl_step_create_clean_dam.print_step_output()
		self.etl_step_create_clean_excel.print_step_output()

		print('\nElectrical ETL process finished!')

	def run_etl_process(self):
		"""This function will run the Electrical ETL process.
		:return: Void."""
		timer = atr.EasyTimer()

		self.etl_step_parse_products.start()
		self.etl_step_parse_assets.start()

		self.etl_step_parse_products.join()
		self.etl_step_parse_products.print_step_output()

		self.etl_step_parse_assets.join()
		self.etl_step_parse_assets.print_step_output()

		# Product manager will map assets to clean products.
		self.product_manager = product_manager.ElectricalProductManager(self.etl_step_parse_products.get_output(), self.etl_step_parse_assets.get_output())
		self.etl_step_map_assets_to_clean_products = ETL_MapAssetsToCleanProducts(self.product_manager)
		self.etl_step_map_assets_to_clean_products.start()
		self.etl_step_map_assets_to_clean_products.join()
		self.etl_step_map_assets_to_clean_products.print_step_output()

		# Temporarily disabled.
		self.etl_step_create_csv_and_dam_to_send = ETL_CreateCSVAndDAM(self.product_manager)
		self.etl_step_create_csv_and_dam_to_send.start()
		self.etl_step_create_csv_and_dam_to_send.join()
		self.etl_step_create_csv_and_dam_to_send.print_step_output()

		print('\n' + str(timer) + '\nElectrical ETL process finished!')

	def run_post_etl_process(self):
		"""This function will run the post ETL process. It does everything AEM side.
		:return: Void."""
		self.etl_step_transfer_files_to_aem = ETL_TransferLocalFilesToAEM()
		self.etl_step_create_sub_packages   = ETL_CreateSubPackages(self.product_manager)

		self.etl_step_create_sub_packages.start()
		self.etl_step_transfer_files_to_aem.start()

		self.etl_step_create_sub_packages.join()
		self.etl_step_transfer_files_to_aem.join()

		self.etl_step_create_sub_packages.print_step_output()
		self.etl_step_transfer_files_to_aem.print_step_output()

		print('\nElectrical post ETL process finished!')

electrical_etl = ElectricalETL()
#electrical_etl.run_pre_etl_process()
electrical_etl.run_etl_process()
electrical_etl.run_post_etl_process()


