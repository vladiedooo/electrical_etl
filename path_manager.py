# coding=utf-8

"""This module, path_manager.py, is used to store all relevant paths, local and non-local ones."""

# Used for getting this file's path.
import os
# Used for checking if a file exists.
import pathlib
# Used to get ETL step names.
from etl_steps import etl_step

# ETL Steps (copied from etl_steps.etl_step.py)
ETL_STEP_CREATE_CLEAN_DAM               = 'create_clean_dam'
ETL_STEP_CREATE_CLEAN_EXCEL             = 'create_clean_excel'
ETL_STEP_QUICK_STEP                     = 'quick_step'
ALL_ETL_STEPS                           = [ETL_STEP_CREATE_CLEAN_DAM, ETL_STEP_CREATE_CLEAN_EXCEL, ETL_STEP_QUICK_STEP]

'''       __   __                __       ___       __
	|    /  \ /  `  /\  |       |__)  /\   |  |__| /__`
	|___ \__/ \__, /~~\ |___    |    /~~\  |  |  | .__/
'''
_CURRENT_PATH                           = os.path.dirname(os.path.realpath(__file__))
_ELECTRICAL_CONTENT_FILES               = _CURRENT_PATH + '/electrical_content_files'
_ETL_STEPS_DIRECTORY                    = _CURRENT_PATH + '/etl_steps'
ETL_STEP_RUNNER_PATH                    = _ETL_STEPS_DIRECTORY + '/run_etl_step.py'
_ETL_STEP_TO_FILE_PATH                  = {ETL_STEP_CREATE_CLEAN_DAM   : _ETL_STEPS_DIRECTORY + '/etl_step_create_clean_dam.py',
                                           ETL_STEP_CREATE_CLEAN_EXCEL : _ETL_STEPS_DIRECTORY + '/etl_step_create_clean_excel.py',
                                           ETL_STEP_QUICK_STEP         : _ETL_STEPS_DIRECTORY + '/etl_step_quick_step.py'}

USE_GENERIC_IMAGE_SOURCE_PATH           = _ELECTRICAL_CONTENT_FILES + '/backup/use_generic_image.jpg'
USE_GENERIC_IMAGE_DAM_CLEANED_PATH      = _ELECTRICAL_CONTENT_FILES + '/dam_cleaned/generic_image_path/use_generic_image.jpg'
USE_GENERIC_IMAGE_DAM_CLEANED_DIRECTORY = _ELECTRICAL_CONTENT_FILES + '/dam_cleaned/generic_image_path/'
USE_GENERIC_IMAGE_DAM_FOR_AEM_PATH      = _ELECTRICAL_CONTENT_FILES + '/dam_for_aem/generic_image_path/use_generic_image.jpg'
USE_GENERIC_IMAGE_DAM_FOR_AEM_DIRECTORY = _ELECTRICAL_CONTENT_FILES + '/dam_for_aem/generic_image/path/'

DAM_ORIGINAL_DIRECTORY                  = _ELECTRICAL_CONTENT_FILES + '/dam_original'
DAM_CLEANED_DIRECTORY                   = _ELECTRICAL_CONTENT_FILES + '/dam_cleaned'
DAM_FOR_AEM_DIRECTORY                   = _ELECTRICAL_CONTENT_FILES + '/dam_for_aem'
DAM_FOR_AEM_COMPRESSED_DIRECTORY        = _ELECTRICAL_CONTENT_FILES + '/dam_for_aem_compressed'

EXCEL_ORIGINAL_PATH                     = _ELECTRICAL_CONTENT_FILES + '/electrical_original.xlsx'
EXCEL_CLEANED_PATH                      = _ELECTRICAL_CONTENT_FILES + '/electrical_cleaned.xlsx'
CSV_FOR_AEM_PATH                        = _ELECTRICAL_CONTENT_FILES + '/csv_for_aem.csv'

PACKAGE_WORKFLOW_ORIGINAL               = _ELECTRICAL_CONTENT_FILES + '/packages/the_dam_assets_workflow_original.zip'
PACKAGE_WORKFLOW_EMPTY                  = _ELECTRICAL_CONTENT_FILES + '/packages/the_dam_assets_workflow_empty.zip'
DOWNLOADED_PACKAGES_DIRECTORY           = _ELECTRICAL_CONTENT_FILES + '/packages/downloaded/'

DOCKER_AEM_PATH                         = '/aem/crx-quickstart/bin/electrical/'

'''       ___                __        __
	 /\  |__   |\/|    |  | |__) |    /__`
	/~~\ |___  |  |    \__/ |  \ |___ .__/
'''
AEM_LOCAL_AUTHOR                        = 'http://localhost:4502/'
AEM_POST_URL_FOR_CSV_ASSET_IMPORT       = 'acs-tools/components/csv-asset-importer'


def get_etl_path(etl_step_name):
	"""Gets the path of the ETL python file given an ETL step name.
	:param etl_step_name: The ETL step name to get the python file path of.
	:return: The path to that python ETL file."""
	return _ETL_STEP_TO_FILE_PATH[etl_step_name]


def does_file_exist_at_(full_path_to_file):
	"""Checks if a file exists from the provided full file path.
	:param full_path_to_file: The full file path to check against.
	:return: Boolean indicating if a file was found at that path or not."""
	if pathlib.Path(full_path_to_file).is_file():
		return True
	return False
