# coding=utf-8

"""This module, dam_for_aem_generator.py, will create the dam that will be sent to AEM."""


# Used for system operations.
import os
# Used for recursively searching through a directory.
import glob
# Used for getting needed paths.
import path_manager as pm
# Used for AEM name compliance.
from aem import aem_api
# Used for creating directories.
import pathlib
# Used for copying files.
import shutil


class DAMForAEMGenerator:
	"""Creates the dam that will be sent to AEM."""

	def __init__(self, assets_needed):
		self.assets_needed = assets_needed

	def generate_dam_to_send(self):
		"""Creates the dam to send.
		:return: Void."""

		file_names = []
		directory_names = []

		for e in glob.glob(pm.DAM_CLEANED_DIRECTORY + '/**', recursive = True):
			if os.path.isdir(e):
				directory_names.append(e)
			else:
				file_names.append(e)

		for d in directory_names:
			# Ignore empty directories.
			if len(os.listdir(d)) > 0:
				new_directory = d.replace('electrical_content_files/dam_cleaned', 'electrical_content_files/dam_for_aem')
				pathlib.Path(new_directory).mkdir(parents = True, exist_ok = True)

		for f in file_names:

			for a in self.assets_needed:
				if a in f:
					directory_name = f[0:f.rfind('/') + 1]
					directory_name = directory_name.replace('electrical_content_files/dam_cleaned', 'electrical_content_files/dam_for_aem')

					file_name = f[f.rfind('/') + 1:]
					file_name = aem_api.get_aem_dam_compliant_version_of_provided_string(file_name, ['.'])

					shutil.copy2(f, directory_name + file_name)
