# coding=utf-8

"""This module, image_compressor.py, will compress jpeg images, more features to be included in the future."""

# Used for IDE auto-complete.
from typing import List
# Used to represent Electrical products.
from electrical_classes.product import ElectricalProduct
# Used to represent Electrical assets.
from electrical_classes.dam_asset import ElectricalAsset
# Used for running compression.
from PIL import Image
# Used for OS operations.
import os
# Used for recursively getting files from a directory.
import glob
from glob2 import iglob
# Used to get useful paths.
import path_manager as pm
# Used for creating directories.
import pathlib
# Used for copying files.
import shutil

# TODO : Add compression for other files types here.


'''
# Make the required directories for the cleaned dam.
for d in directory_names:
	# Ignore empty directories.
	if len(os.listdir(d)) > 0:
		clean_directory_name = aem_api.get_aem_dam_compliant_version_of_provided_string(d, ['/']).replace('electrical_content_files/dam_original', 'electrical_content_files/dam_clean')
		pathlib.Path(_get_clean_directory_name(d)).mkdir(parents=True, exist_ok=True)

# Make the directory for the generic image.
pathlib.Path(pm.USE_GENERIC_IMAGE_DAM_CLEANED_DIRECTORY).mkdir(parents=True, exist_ok=True)

# Now copy over the asset files from the dam original.
for f in file_names:
	directory_name = f[0:f.rfind('/') + 1]
	directory_name = _get_clean_directory_name(directory_name)

	file_name      = f[f.rfind('/') + 1:]
	file_name      = aem_api.get_aem_dam_compliant_version_of_provided_string(file_name, ['.'])

	shutil.copy2(f, directory_name + file_name)

# Transfer the generic image.
shutil.copy2(pm.USE_GENERIC_IMAGE_SOURCE_PATH, pm.USE_GENERIC_IMAGE_DAM_CLEANED_PATH)
'''


class ImageCompressor:
	"""This class compresses image files for specified image formats."""

	def _get_file_size(self, file_path) -> str:
		"""Return the size of the file.
		:param file_path : The full path to the file.
		:return: The size of the file as a string."""
		return str(int(int(os.stat(file_path).st_size) / 1024))

	def _compress_individual_asset(self, asset_path, compressed_file_save_path):
		"""Compresses a single asset / image file.
		:param asset_path : The full path to the image file.
		:param compressed_file_save_path : The full path of where to place the new compressed image file.
		:return: Void."""
		try:
			asset_size = self._get_file_size(asset_path)
			#print('The file size before : ' + asset_size)

			foo           = Image.open(asset_path)
			width, height = foo.size
			if int(asset_size) > 1200:
				foo = foo.resize((int(width / 2), int(height / 2)), Image.ANTIALIAS)
			if int(asset_size) > 500:
				foo.save(compressed_file_save_path, quality=75, optimize=True)
			else:
				foo.save(compressed_file_save_path, quality=85, optimize=True)
			after_size = self._get_file_size(compressed_file_save_path)
			#print('The file size after : ' + self._get_file_size(compressed_file_save_path))
			#if int(after_size) > 500:
			#	print('BAD')
			#	print(after_size)
		except Exception as e:
			print('Error trying to compress a file! Exception{' + str(e) + '}')

	def compress_assets_in_directory(self, directory_path):
		"""Compresses all compatible assets types found in this directory.
		:param directory_path : The directory path to compress found assets in.
		:return: Void."""
		for filename in glob.glob(pm.DAM_FOR_AEM_DIRECTORY + '/**', recursive=True):
			# Ignore files, only look at directories.
			if os.path.isdir(filename):
				directory_name = filename.replace('electrical_content_files/dam_for_aem', 'electrical_content_files/dam_for_aem_compressed')
				pathlib.Path(directory_name).mkdir(parents=True, exist_ok=True)

		for filename in glob.glob(pm.DAM_FOR_AEM_DIRECTORY + '/**', recursive=True):
			# Ignore directories, only look at files.
			if not os.path.isdir(filename):
				print('Compressing : ' + str(filename))

				relative_path = filename.replace('/Users/utarsuno/git_repos/electrical_etl/electrical_content_files/dam_for_aem/', '')
				file_name     = relative_path[relative_path.rfind('/') + 1:]
				relative_path = relative_path[0:relative_path.rfind('/') + 1]

				self._compress_individual_asset(filename, pm.DAM_FOR_AEM_COMPRESSED_DIRECTORY + '/' + relative_path + file_name)

	# TODO :
	def compress_assets(self, products: List[ElectricalProduct]):
		y = 2




