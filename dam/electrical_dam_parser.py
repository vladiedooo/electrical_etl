# coding=utf-8

"""This module, electrical_dam_parser.py, parses the contents of the clean DAM directory."""

# Used for recursively getting files from a directory.
import glob
from glob2 import iglob
# Used to represent electrical asset files.
from electrical_classes.dam_asset import ElectricalAsset
# Used for OS operations.
import os
# To convert values to human readable formats.
import humanize
# Used to help the IDE.
from typing import List


def get_easy_to_read_size_of_directory(directory):
	"""Returns the size of the directory converted to the most logical unit.
	:return: A string representation of the size of the provided directory."""
	return str(humanize.naturalsize(sum(os.path.getsize(x) for x in iglob(directory + '/**'))))


def get_raw_size_as_bytes_of_directory(directory):
	"""Returns the "RAWWWWWWWW"-(Gordan Ramsey) size of the directory.
	:return: A string representation of the size of the provided directory."""
	return str(sum(os.path.getsize(x) for x in iglob(directory + '/**')))


class ElectricalDAMParser:
	"""Parses the Electrical DAM files."""

	def __init__(self, dirty_dam_path, clean_dam_path):
		self.dirty_dam_path       = dirty_dam_path
		self.clean_dam_path       = clean_dam_path
		self.assets               = []

	def parse_out_assets(self):
		"""Get information about the assets files.
		:return: Void."""
		for filename in glob.glob(self.clean_dam_path + '/**', recursive=True):
			# Ignore directories, only look at files.
			if not os.path.isdir(filename):
				self.assets.append(ElectricalAsset(filename))
		return self.assets

